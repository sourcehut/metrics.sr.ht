# metrics.sr.ht

This repository tracks our Prometheus alert rules. They are available as a
package from mirror.sr.ht (for Alpine only) as metrics.sr.ht-rules.

Our Prometheus instance is public:

https://metrics.sr.ht

## Usage instructions

1. Install our package
2. Add our `rules_files` entries to your `prometheus.yml` for each set of rules
   you wish to use
3. Configure alertmanager accordingly

Our alerts are categorized into three severity groups:

- **interesting** alerts are worth noting, as they may be useful in identifying
  trends over time, for forensic attention after an outage, or for addressing on
  a rainy day. Upstream, we send these to our IRC channel.
- **important** alerts are likely to be actionable, but do not require immediate
  attention.
- **urgent** alerts require immediate attention.
